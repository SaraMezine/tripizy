
INSERT INTO defaultdb.articles (id, continent, country, city, type,
                                title, pseudo, description, date, id_photo) VALUES
(
    1, 'Asie', 'Malaisie', 'Kuala Lumpur', 'Restaurant', 'Burger On 16', 'Fazila', 'At Burger On 16, great burgers are served. Grilling a fresh, premium cut, hand-packed patties on a hot seasoned grill to perfection with the perfect char taste results in a delicious juiciness burger that talks about itself. Add butter-toasted buns, high grade cheeses, freshly sliced tomato and onions, with intense sauces made in-house, and you’ve got a burger that twist your mind!', CURDATE(), 'burger-kuala-lumpur.jpg'
);

INSERT INTO defaultdb.articles (id, continent, country, city, type,
                                title, pseudo, description, date, id_photo) VALUES
(
    2, 'Asie', 'Malaisie', 'Kuala Lumpur', 'Activite', 'Batu Cave', 'Gilbert', 'Site d un temple et d un sanctuaire hindou, les grottes de Batu attirent des milliers de fidèles et de touristes, tout particulièrement pendant le festival annuel hindou, le Thaipusam.
Affleurement calcaire situé  juste au nord de Kuala Lumpur, les grottes de Batu se composent de trois grottes principales accueillant des temples et des sanctuaires hindous.', CURDATE(), 'batu-caves-statue.jpg'

);

INSERT INTO defaultdb.articles (id, continent, country, city, type,
                                title, pseudo, description, date, id_photo) VALUES
(
    3, 'Asie', 'Malaisie', 'Kuala Lumpur', 'Hotel / Airbnb', 'The FACE Platinium', 'Antoine', 'The FACE Platinium is an all-suite hotel concept comprising of 200 units of sheer luxury with an inspired design and attention to details with a stunning view of the Kuala Lumpur city skyline. Designed to cater to the needs of evey business and leisure travellers. Each suite feature functional space for work or study, modern living room and en suite bathroom as well as fully equipped kitchenette with modern appliance.', CURDATE(), 'the_face.jpg'
);

INSERT INTO defaultdb.articles (id, continent, country, city, type,
                                title, pseudo, description, date, id_photo) VALUES
(
    4, 'Asie', 'Malaisie', 'Perhentian', 'Activite', 'Plage paradisiaque', 'Sara', 'Situées au Nord Est de la Malaisie, à 19km des côtes, les îles Perhentians font partie des plus belles îles et plages de Malaisie. Bien que touristiques, elles abritent des plages magnifiques et disposent de fonds marins exceptionnels. Des îles idéales donc pour faire de la plongée et du snorkeling en Malaisie. ', CURDATE(), 'perhantian.jpg'
);

INSERT INTO defaultdb.articles (id, continent, country, city, type,
                                title, pseudo, description, date, id_photo) VALUES
(
    5, 'Amerique', 'Etats Unis', 'New york', 'Restaurant', 'Maman', 'Serena', 'In williamsburg', CURDATE(), 'IMG_0125.jpeg'
);


INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('France', 'description sur la France', 'europe','france.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Italie', 'description sur Italie', 'europe','italie.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Argentine', 'description sur Argentine', 'ameriques','argentine.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Australie', 'description sur Australie', 'oceanie','australie.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Egypte', 'description sur Egypte', 'afrique','egypte.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Japon', 'Longtemps, le Japon a ete assimile a une sorte de planete Mars, et pourtant, c est une etoile fascinante', 'asie','japon.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Etats-unis', 'description sur les Etats-unis', 'ameriques','etats-unis.jpeg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Grece', 'description sur la Grèce', 'europe','grece.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Inde', 'L Inde est un pays pas comme les autres, qui vous promet un voyage vers des espaces inconnus', 'asie','inde.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Kenya', 'description sur le Kenya', 'afrique','kenya.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Malaisie', 'La Malaisie est un pays aux multiples facettes, tant sur le plan politique, religieux, humain que geographique.', 'asie','malaisie.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Maroc', 'description sur le Maroc', 'afrique','maroc.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Mexique', 'description sur le Mexique', 'ameriques','mexique.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Nouvelle-Zelande', 'description sur la Nouvelle-Zelande', 'oceanie','nouvelle-zelande.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Portugal', 'description sur le Portugal', 'europe','portugal.jpg');
INSERT INTO defaultdb.countries (name, description,continent,nom_image) VALUES ('Afrique du Sud', 'description sur Afrique du Sud', 'afrique','afrique-du-sud.jpg');


INSERT INTO defaultdb.users (first_name, last_name, age, pseudo, password, address_mail) VALUES ("Laurie","Vulcain",22,"lolos","05051997","vulcain.laurie@gmail.com");

INSERT INTO defaultdb.comments(id, pseudo_author, id_article, date_publication, content) VALUES (1, 'lolo', 1, CURDATE(), 'des super burger et frites');
INSERT INTO defaultdb.comments(id, pseudo_author, id_article, date_publication, content) VALUES (2, 'lulu', 1, CURDATE(), 'Jai adoweee mange des burugurrrr wow');
INSERT INTO defaultdb.comments(id, pseudo_author, id_article, date_publication, content) VALUES (3, 'sara', 1, CURDATE(), 'Jai trouver mieux quand meme');


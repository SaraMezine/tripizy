create table articles
(
    id bigint auto_increment,
    constraint articles
        primary key (id),
    continent TEXT not null,
    country TEXT not null,
    city TEXT not null,
    type TEXT not null,
    title TEXT not null,
    pseudo TEXT not null,
    description TEXT not null,
    date DATE not null,
    id_photo TEXT not null

);

create table countries
(
    id bigint auto_increment,
    constraint countries_pk
        primary key (id),
    name TEXT not null,
    description TEXT not null,
    continent TEXT not null,
    nom_image TEXT null
);
create table users
(
    id bigint auto_increment,
    constraint countries_pk
        primary key (id),
    first_name TEXT not null,
    last_name TEXT not null,
    age int not null,
    pseudo TEXT null,
    password TEXT not null,
    address_mail TEXT not null
);

create table comments
(
    id bigint auto_increment,
    constraint countries_pk
        primary key (id),
    pseudo_author TEXT not null,
    id_article INT not null,
    date_publication DATE not null,
    content TEXT null
);




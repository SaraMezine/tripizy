# Tripizy

MEZINE Sara – sara.mezine@epfedu.fr
POQUET Ludivine – ludivine.poquet@epfedu.fr
VULCAIN Laurie – laurie.vulcain@epfedu.fr 
EPF - P2020 – MIN1 – Projet java dev&ops

Tripizy

Notre projet consiste en un site internet ayant pour thème les voyages. Ce site rassemble un ensemble d’articles sur différentes destinations proposées par des utilisateurs. Ces articles ont pour but de donner des conseils sur des restaurants, logements, activités à des personnes ayant envie de visiter ces endroits. Les articles sont proposés par des utilisateurs ayant visité la destination, et sont destinés aux futurs voyageurs en quête de renseignements.

Pour récupérer notre dossier, veuillez entrer la commande suivante dans le dossier choisi : 
Git clone https://gitlab.com/SaraMezine/tripizy.git 

Applications.properties
Afin de garantir le bon fonctionnement du projet, il est nécessaire d’ajouter dans votre dossier /ressources, un fichier graddle nommé application.properties.

Après avoir créé ce fichier, veuillez y insérer :

Pour Windows :
spring.thymeleaf.cache=false
spring.datasource.url=jdbc:mariadb://192.168.99.100/defaultdb
spring.datasource.username=root
spring.datasource.password=toor
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver
spring.resources.static-locations=classpath:/static/
spring.servlet.multipart.max-file-size=300MB
spring.servlet.multipart.max-request-size=300MB

Pour MAC : 
spring.thymeleaf.cache=false
spring.datasource.url=jdbc:mariadb://localhost/defaultdb
spring.datasource.username=root
spring.datasource.password=toor
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver
spring.resources.static-locations=classpath:/static/
spring.servlet.multipart.max-file-size=300MB
spring.servlet.multipart.max-request-size=300MB

Initialisation de la base de données 
Une bas de données mariaDB est nécessaire pour faire fonctionner le site. Il faut donc l’initialiser dans un docker. 
Pour cela ouvrez votre docker (terminal pour MAC) dans le dossier du projet et lancer la commande suivante :
docker run --name mariadb --rm -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_DATABASE=defaultdb -p 3306:3306 -v "pwd/initdb:/docker-entrypoint-initdb.d" mariadb


Fonctionnalités et tests
Les principales fonctionnalités de notre site sont :
-	L’accès à des articles par un simple utilisateur non identifié
Les articles sont accessibles de différentes manières, par la barre de navigation qui vous emmènera vers une liste de pays selon le continent choisi ou en cliquant sur l’une des destinations du moment contenue dans le carrousel.
Nous avons choisi de nous concentrer sur le site en lui-même et non sur son contenu. Nous n’avons donc pas rempli chaque destination avec des articles, ainsi qu’utilisé un générateur de texte afin de créer du contenu. Néanmoins vous pouvez tester une liste d’articles complets en suivant le chemin suivant : Asie/Malaisie.
-	L’inscription et la connexion à un espace d’utilisateur enregistré ainsi que la déconnexion
Nous avons décidé de restreindre certaines fonctionnalités à des utilisateurs enregistrés, comme l’écriture et la modification d’articles.
Afin de tester cette fonctionnalité, nous vous conseillons de tester tout d’abord l’inscription, puis la déconnexion puis la connexion.
-	La rédaction d’articles 
Connectez-vous avec vos identifiants, cliquez sur « Nouvel Article » dans la barre de navigation, puis validez. Après rédaction, Il sera disponible via le chemin indiqué (continent/pays). 
-	Le tri des articles selon leur sujet
Le tri est possible selon trois sujets différents : les logements, les restaurants et les activités. 
Nous vous conseillons de retrouver le chemin : Asie/Malaisie pour tester ce tri.
-	L’ajout de commentaires aux articles 
L’ajout des commentaires se fait en bas de page d’un article. Cette fonctionnalité est disponible pour les utilisateurs enregistrés ou non, et sont visibles par tous.
-	La possibilité de modifier un article que l’on a écrit pour un utilisateur enregistré
Afin de tester cette fonctionnalité, connectez-vous avec vos identifiants entrés lors de l’inscription. Puis cliquez sur l’onglet « Mes Articles », vos articles auront le bouton « modifier » disponible. Si vous voulez modifier une information, remplissez les champs avec les nouvelles informations. 

Bibliographie
Nous avons essayé d’utiliser un maximum d’images libres de droit afin de remplir notre site.
Les « faux » textes ont été générés en utilisant le site : https://www.faux-texte.com/

Voici quelques sites qui nous ont permis d’ajouter du contenu : 
http://etats-unis.americas-fr.com/new-york.html
https://japonais.eu/Le_Japon/Tokyo.php
https://www.ef.fr/blog/language/cape-town-ville-la-plus-formidable-au-monde-5-raisons/

package fr.takima.demo.model

import java.util.*
import javax.persistence.*

/**
 *
 */

@Entity(name = "comments")
data class Comments(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id: Long?,
        @Column(name = "pseudo_author") var pseudo_author: String?,
        @Column(name = "id_article") var id_article: Int?,
        @Column(name = "date_publication") var date_publication: Date?,
        @Column(name = "content") var content: String?) {
    constructor() : this(null, null, null, null, null)

}
package fr.takima.demo.model

import java.util.*
import javax.persistence.*

/**
 *
 */

@Entity(name = "articles")
data class Article(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id: Long?,
        @Column(name = "continent") var continent: String?,
        @Column(name = "country") var country: String?,
        @Column(name = "city") var city: String?,
        @Column(name = "type") var type: String?,
        @Column(name = "title") var title: String?,
        @Column(name = "pseudo") var pseudo: String?,
        @Column(name = "description") var description: String?,
        @Column(name = "date") var date: Date?,
        @Column(name = "id_photo") var id_photo: String?)
{
    constructor() : this(null, null, null, null,null,null,null,
            null, null, null)

}
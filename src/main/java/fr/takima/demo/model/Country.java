package fr.takima.demo.model;

import javax.persistence.*;
import java.util.Objects;
@Entity
@Table(name="countries")
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String description;
    private String continent;
    private String nomImage;

    public Country(String name, String description, String continent) {
        this.name = name;
        this.description = description;
        this.continent = continent;
        this.nomImage=name+".jpg";
    }

    public Country() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getNomImage() {
        return nomImage;
    }

    public void setNomImage(String nomImage) {
        this.nomImage = nomImage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;
        Country country = (Country) o;
        return name.equals(country.name) &&
                description.equals(country.description) &&
                continent.equals(country.continent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, continent);
    }
}

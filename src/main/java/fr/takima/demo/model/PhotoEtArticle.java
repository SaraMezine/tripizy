package fr.takima.demo.model;

import org.springframework.web.multipart.MultipartFile;

public class PhotoEtArticle {
    private Article unArticle;
    private MultipartFile filePhotoUpload;

    public PhotoEtArticle() {
        unArticle=new Article();
    }

    public Article getUnArticle() {
        return unArticle;
    }

    public void setUnArticle(Article unArticle) {
        this.unArticle = unArticle;
    }

    public MultipartFile getFilePhotoUpload() {
        return filePhotoUpload;
    }

    public void setFilePhotoUpload(MultipartFile filePhotoUpload) {
        this.filePhotoUpload = filePhotoUpload;
    }
}

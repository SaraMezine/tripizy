 package fr.takima.demo.dao;

import fr.takima.demo.model.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

 /**
 *
 */
@Repository
public interface CountryDAO extends CrudRepository<Country, Long> {
Country findByName(String name);
Country[] findAllByContinent(String continent);

}

package fr.takima.demo.dao;

import fr.takima.demo.model.Comments;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentsDAO  extends CrudRepository<Comments, Long> {

    @Query(value = "SELECT * FROM comments c WHERE c.id_article = ?1", nativeQuery = true)
    List<Comments> findAllById_article(int Id_article);

}

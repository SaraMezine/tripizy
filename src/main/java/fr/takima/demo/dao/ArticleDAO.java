package fr.takima.demo.dao;

import fr.takima.demo.model.Article;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;



@Repository
public interface ArticleDAO extends CrudRepository<Article, Long> {

    @Query(value = "SELECT * FROM articles a WHERE a.type = ?1 ", nativeQuery = true)
    Collection<Article> findAllByType(String type_article);

    @Query("SELECT a FROM articles a WHERE a.id = ?1")
    Article findArticleById(Long id_article);

    @Query(value = "SELECT * FROM articles a WHERE a.country = ?1", nativeQuery = true)
    List<Article> findAllByCountry(String country_name);

    List<Article> findAllByPseudo(String pseudo);

    List<Article> findAllByTypeAndCountry(String type, String country);
}

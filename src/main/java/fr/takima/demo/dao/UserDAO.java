package fr.takima.demo.dao;

import fr.takima.demo.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDAO extends CrudRepository<User, Long> {
    //Connexion
    User findByPasswordAndPseudo(String password, String pseudo);
    User findByPseudo(String pseudo);
}

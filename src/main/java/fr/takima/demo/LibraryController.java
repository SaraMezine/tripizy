package fr.takima.demo;


import fr.takima.demo.dao.ArticleDAO;
import fr.takima.demo.dao.CommentsDAO;
import fr.takima.demo.dao.CountryDAO;
import fr.takima.demo.dao.UserDAO;
import fr.takima.demo.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;



/**
 *
 */
@RequestMapping("/")
@Controller
public class LibraryController {

  private final ArticleDAO articleDAO;
  private final CommentsDAO commentsDAO;
  private List<String> type_article = new ArrayList<>();
  List<Article> article_selection = new ArrayList<>();
  SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
  Article selected_article = new Article();
  private final CountryDAO countryDAO;
  private final UserDAO userDAO;
  private User theUser;


  public LibraryController(ArticleDAO articleDAO,CountryDAO countryDAO, CommentsDAO commentsDAO,UserDAO userDAO) {
    this.articleDAO = articleDAO;
    this.userDAO = userDAO;
    type_article.add("Activite");
    type_article.add("Restaurant");
    type_article.add("Hotel / Airbnb");
    this.countryDAO = countryDAO;
    this.commentsDAO = commentsDAO;
    this.theUser=new User();
    theUser.setFirstName("Non renseigné");
  }

  @GetMapping
  public String homePage(Model m) {
      m.addAttribute("theUser",theUser);
      return "home";

  }

  @GetMapping("/new")
  public String addUserPage(Model m) {
      m.addAttribute("theUser",theUser);
      m.addAttribute("photoarticle",new PhotoEtArticle());
        return "new";

  }
    // POST: Do Upload
    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public RedirectView uploadOneFileHandlerPOST(HttpServletRequest request, //
                                                 Model model, @ModelAttribute PhotoEtArticle photoEtArticle, RedirectAttributes attrs) {
        Article article=photoEtArticle.getUnArticle();
        MultipartFile file =photoEtArticle.getFilePhotoUpload();
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());

        article.setDate(date);
        article.setPseudo(theUser.getPseudo());
        article.setId_photo(Objects.requireNonNull(file.getOriginalFilename()));
        articleDAO.save(article);
        attrs.addFlashAttribute("message", "Utilisateur ajouté avec succès");
        return this.doUpload(file);

    }


    private RedirectView doUpload(MultipartFile file) {
        // Root Directory.
        Path path = FileSystems.getDefault().getPath("src","main","resources","static","images");
        String pathToString=path.toString();
        File uploadRootDir = new File(pathToString);
        // Create directory if it not exists.
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }

        // Client File Name
        String name = file.getOriginalFilename();
        System.out.println("Client File Name = " + name);

        if (name != null && name.length() > 0) {
            try {
                // Create the file at server
                File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);

                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(file.getBytes());
                stream.close();
                //

                System.out.println("Write file: " + serverFile);
                //Re do cause it doesn't go to the target directory so the photo isn't available
                // Root Directory.
                Path path2 = FileSystems.getDefault().getPath("target","classes","static","images");
                String pathToString2=path2.toString();
                File uploadRootDir2 = new File(pathToString2);
                // Create directory if it not exists.
                if (!uploadRootDir2.exists()) {
                    uploadRootDir2.mkdirs();
                }
                 serverFile = new File(uploadRootDir2.getAbsolutePath() + File.separator + name);
                stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(file.getBytes());
                stream.close();

            } catch (Exception e) {
                System.out.println("Error Write file: " + name);
            }
        }
        return new RedirectView("/");
    }

  @PostMapping("article/{id}")
  public String createNewComment(@ModelAttribute Comments comments, @PathVariable Long id, Model m){

      Date date = new Date();
      comments.setDate_publication(date);
      comments.setId_article(id.intValue());
      commentsDAO.save(comments);

      m.addAttribute("theUser",theUser);
      m.addAttribute("selected_article", articleDAO.findArticleById(id));
      m.addAttribute("Comments", new Comments());
      m.addAttribute("list_comments", commentsDAO.findAllById_article(id.intValue()));
      m.addAttribute("title", articleDAO.findArticleById(id).getTitle());

      return "article";
  }


   @GetMapping("/articles")
  public String getArticlesCountry(Model m) {
       m.addAttribute("theUser",theUser);
    m.addAttribute("article_pays", articleDAO.findAll());
    m.addAttribute("type_article", type_article);
    return "list_article";
  }

  @GetMapping(value = "/article/{id}")
  public String getArticleByID(@PathVariable Long id, Model m){
      m.addAttribute("theUser",theUser);
    m.addAttribute("selected_article", articleDAO.findArticleById(id));
    m.addAttribute("Comments", new Comments());
    m.addAttribute("list_comments", commentsDAO.findAllById_article(id.intValue()));
    m.addAttribute("title", articleDAO.findArticleById(id).getTitle());
    return "article";
  }


  @GetMapping(value = "/articles/{country_name}")
  public String getArticleByCountry(@PathVariable String country_name, Model m){
      m.addAttribute("theUser",theUser);
      m.addAttribute("article_pays", articleDAO.findAllByCountry(country_name));
      m.addAttribute("type_article", type_article);
      m.addAttribute("country", country_name);
      return "list_article";
  }

    @PostMapping(value = "/articles/{country_name}")
    public String getArticleByCountryandType(@PathVariable String country_name, @RequestParam("idChecked") List<String> type_selection, Model m){
        article_selection.clear();
        if(type_selection != null){
            for(String type : type_selection){
                article_selection.addAll(articleDAO.findAllByTypeAndCountry(type, country_name));
            }
        }

        m.addAttribute("theUser",theUser);
        m.addAttribute("article_pays", article_selection);
        m.addAttribute("type_article", type_article);
        m.addAttribute("country", country_name);
        return "list_article";
    }

  @GetMapping(value = {"/asie","/afrique","/europe","/ameriques","/oceanie"})
  public String showCountries(Model m, HttpServletRequest request){
    String url=request.getRequestURL().toString();
    String[] leContinent=url.split("http://localhost:8080/");

      m.addAttribute("theUser",theUser);
    m.addAttribute("continent",leContinent[1]);
    m.addAttribute("description_continent",selectDescriptionContinent(leContinent[1]));
    m.addAttribute("countries",countryDAO.findAllByContinent(leContinent[1]));

    return "continentPage";
  }

  private String selectDescriptionContinent(String continent){
      String description="";
      switch (continent){
          case "asie":
              description="Découvrir l'Asie c'est tout simplement aller à la rencontre de ce que la nature a fait de plus beau, une richesse naturelle incroyable qui en fait un lieu paradisiaque !\n" +
                      "\n" +
                      "\n" +
                      "Son relief abrite le plus haut sommet du monde, le mont Everest qui culmine à 8 848 mètres mais aussi des points très bas comme au niveau de la Mer morte avec 400 m en-dessous du niveau de la mer.\n" +
                      "\n" +
                      "\n" +
                      "Chacun y trouvera son compte. Voyager en Asie offre une multitude de trésors. Des ruines, des paysages et surtout une modernité qui laisse bouche bée.\n" +
                      "\n" +
                      "\n" +
                      "De pays en pays, ce sont les siècles que nous traversons, passant d'une Asie ancestrale à une Asie futuriste...\n" +
                      "\n" +
                      "\n" +
                      "Bon voyage dans les couloirs du temps!";
              break;
          case "ameriques":
              description="Quam ob rem id primum videamus, si placet, quatenus amor in amicitia progredi debeat. Trop bien les Ameriques ! Numne, si Coriolanus habuit amicos, ferre contra patriam arma illi cum Coriolano debuerunt? num Vecellinum amici regnum adpetentem, num Maelium debuerunt iuvare?";
              break;
          case "europe":
              description="Quam ob rem id primum videamus, si placet, quatenus amor in amicitia progredi debeat. Numne, si Coriolanus habuit amicos, ferre contra patriam arma illi cum Coriolano debuerunt? Vive l'Europe ! num Vecellinum amici regnum adpetentem, num Maelium debuerunt iuvare?";
              break;
          case "afrique":
              description="Quam ob rem id primum videamus, si placet, quatenus amor in amicitia progredi debeat. Numne, si Coriolanus habuit amicos, ferre contra patriam arma illi cum Coriolano debuerunt? Wow l'Afrique ! num Vecellinum amici regnum adpetentem, num Maelium debuerunt iuvare?";
              break;
          case "oceanie":
              description="Quam ob rem id primum videamus, si placet, quatenus amor in amicitia progredi debeat. Numne, si Coriolanus habuit amicos, ferre contra patriam arma illi cum Coriolano debuerunt? num Vecellinum amici regnum adpetentem, num Maelium debuerunt iuvare? Youhouu Oceanie";
              break;

      }
      return description;

  }


    @GetMapping("/newuser")
    public String addNewUser(Model m) {

        m.addAttribute("theUser",theUser);
        m.addAttribute("user",new User());
        m.addAttribute("firstnameuser", theUser.getFirstName());
        return "new_user";

    }
    @PostMapping("/newuser")
    public RedirectView createNewUser(@ModelAttribute User user, RedirectAttributes attrs) {
        attrs.addFlashAttribute("message", "Utilisateur ajouté avec succès");
        theUser=userDAO.findByPseudo(user.getPseudo());
        if (theUser==null){
            userDAO.save(user);
            theUser=userDAO.findByPasswordAndPseudo(user.getPassword(),user.getPseudo());
            return new RedirectView("/");
        }else {
            attrs.addFlashAttribute("error_creation", "Pseudo déjà utilisé");
            return new RedirectView("/newuser");
        }

    }
    @PostMapping("/connexion")
    public RedirectView connexionUser(@ModelAttribute User user, RedirectAttributes attrs) {
      theUser=userDAO.findByPasswordAndPseudo(user.getPassword(),user.getPseudo());
        if (theUser!=null){
            attrs.addFlashAttribute("message", "Connexion réussie");
            return new RedirectView("/");
        }else {
            attrs.addFlashAttribute("error_connexion", "Utilisateur non trouvé recommencez");
            return new RedirectView("/newuser");
        }


    }
    @GetMapping("/deconnexion")
    public RedirectView deconnexionUser(@ModelAttribute User user, RedirectAttributes attrs) {
        theUser = null;
        theUser= new User();
        theUser.setFirstName("Non renseigné");
        return new RedirectView("/");

    }

    @GetMapping(value = "/new/countriesInContinent")
    public @ResponseBody Country[] findAllCountries(@RequestParam(value = "continentName", required = true) String continent) {
        Country[] countries = countryDAO.findAllByContinent(continent);
        return countries;

    }
    @GetMapping("/mesArticles")
    public String findMyArticles(Model m){
      m.addAttribute("article_pays",articleDAO.findAllByPseudo(theUser.getPseudo()));
      m.addAttribute("theUser",theUser);
        m.addAttribute("type_article", type_article);
      return "list_article";
    }

    @GetMapping(value = "/edit/{id}")
    public String editArticleByID(@PathVariable Long id, Model m){
        m.addAttribute("theUser",theUser);
        m.addAttribute("selected_article", articleDAO.findArticleById(id));
        m.addAttribute("Comments", new Comments());
        m.addAttribute("list_comments", commentsDAO.findAllById_article(id.intValue()));
        m.addAttribute("article",new Article());
        return "edit_article";
    }

    @PostMapping("/edit/{id}")
    public RedirectView postEdit(@PathVariable Long id,@ModelAttribute Article articleEdited, RedirectAttributes attrs, Model m) {
       Article newArticle=articleDAO.findArticleById(id);
       newArticle.setType(articleEdited.getType());
       newArticle.setTitle(articleEdited.getTitle());
       newArticle.setPseudo(articleEdited.getPseudo());
       newArticle.setDescription(articleEdited.getDescription());
        m.addAttribute("theUser",theUser);
       if(articleEdited.getTitle()!="" && articleEdited.getType()!="" && articleEdited.getPseudo()!="" && articleEdited.getDescription()!=""){
           articleDAO.save(newArticle);
           return new RedirectView("/article/"+id);
       }else {
           attrs.addFlashAttribute("error", "Des informations sont manquantes ! Recommencez");
           return new RedirectView("/edit/"+id);
       }

    }
 }
